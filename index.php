
<?php

// First, let's define our list of routes.
// We could put this in a different file and include it in order to separate
// logic and configuration, but its not needed

function router($routes) {
    foreach ($routes as $path => $filePath){
        if($path == $_SERVER['REQUEST_URI']){
            include("./views/blocks/head.php");
            include($filePath);
            include("./views/blocks/footer.php");
            return;
        }
    }
    // api
    if(str_starts_with($_SERVER['REQUEST_URI'], '/api' )){
        include('./db/db_fetch.php');
        return;
    }
    echo 'Sorry! Page not Found!';
}

$routes = array(
    '/'      => './views/productlist/index.php',
    '/add-product' => './views/addproduct/index.php',
);

router($routes);

?>



