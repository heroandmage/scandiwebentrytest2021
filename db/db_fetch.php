<?php 
    require_once("./db/db_connection.php");

    if($PDO){
        if(isset($_GET['sku'])){
            $result = $PDO->prepare('SELECT * FROM product WHERE sku = :sku');
            $result->execute([ 'sku' => $_GET['sku'] ]);
            $fulldata = $result->fetchAll();
            echo json_encode($fulldata);
        }
        else{
            $_POST = json_decode(file_get_contents('php://input'), true);

            if(
                isset($_POST['sku'])            &&
                isset($_POST['name'])           &&
                isset($_POST['price'])          &&
                isset($_POST['detail_type'])    &&
                isset($_POST['detail_value'])
            ){
                $result = $PDO->prepare('INSERT INTO `product` (`sku`, `price`, `detail_type`, `detail_value`, `name`) VALUES (:sku, :price, :detail_type, :detail_value, :name)');
                $result->execute([ 
                    'sku' => $_POST['sku'],
                    'name' => $_POST['name'],
                    'price' => $_POST['price'],
                    'detail_type' => $_POST['detail_type'],
                    'detail_value' => $_POST['detail_value'],
                ]);
                if ( ! $result ){
                    echo "false";
                    exit;
                }
                echo "true";
                exit;
            }

            if(isset($_POST['delete_list'])){

                $ids = $_POST['delete_list']; 
                $sql = 'DELETE FROM product ';
                for($i = 0; $i < count($ids); ++$i) {
                    $result = $PDO->prepare("DELETE FROM product WHERE FIND_IN_SET(sku, :id)");
                    $result->execute(['id' => $ids[$i]]);
                    if ( ! $result ){
                        var_dump( $stmt->errorInfo() );
                        exit;
                    }
                }

                print_r(json_encode($_POST['delete_list']));
                exit;
            }

            
            $sql = "SELECT * FROM `product`";
            $result = $PDO->query( $sql );
            $fulldata = $result->fetchAll();
            echo json_encode($fulldata);
        }
        
    }
    else{
        echo "Something's wrong, no db connections";
    }

?>