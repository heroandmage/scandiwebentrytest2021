/* 
    views\productListPage\render.jsx is the reactjs script for 
    the frontend related to the productlist page, aka, landing page
    the purpose of this file is to fetch, render and compound the ui/ux of the page

    in this file you will find:
    |the fetchapi helper
    |the helping content
    |the product list class
    |    the product list ui state managment
    |    the product list render
    |    the delete post request method
    |the individual item render

*/

type Product = {
    sku: string,
    name: string,
    price: string,
    detail_type: string,
    detail_value: string
}

type SKU = string;
type ProductListViewState = {itemsData: Array<Product>, selectedItemsIds: Array<SKU>};

// fetch all items from rest api with a dynamic callback
const fetchItemsFromApi = (callback: (obj:any) => void) => {
    fetch("api")
    .then( j => {
        j.json()
        .then( callback )
    })
}

// helping content for when there are no products showing
const helpPage = () => <div className="help-page">
        <h3> There is no <i>Product</i> to show ... &#128516; </h3>
        <h4> The reason can be that there are no products registered in our database,</h4>
        <br/>
        
        <h4> or an error with the <strong>SQL</strong> connection</h4>
        <br />
        
        <h4>Try adding a new <i>product</i> by clicking the 
        
        <button
        class="inline-add bx--btn--primary"
        onClick={redirectToAddProduct}
        type="button">
            ADD </button>
        button</h4>
        <br />
        
        <h4> Or <button
        class="inline-add bx--btn--secondary"
        onClick={() => window.location.assign("http://linkedin.com/in/dev-icaroglauco/")}
        type="button">
            Cnntact me </button> so I can do something</h4>
    </div>

// view layer from the productlist page
class ProducListView extends React.Component {

    // items data to render
    // list of selected items ids
    state: ProductListViewState =  {
        itemsData: [],
        selectedItemsIds: [],
    }

    constructor(props){
        super(props);

        // fetch items then trow to the state
        fetchItemsFromApi( (data:Array<Product>) => {
            this.setState( (prev: ProductListViewState) => ({
                ...prev,
                itemsData : data.reverse(),
            }));
        });
    }

    // factory pattern for checkboxs
    setCheckedFac = (id:SKU) => (bool:boolean) => {
        this.setState((prev: ProductListViewState) => {
            // get all the selected items ids
            let selectedItemsIds = [...prev.selectedItemsIds] 

            // the item is being selected
            if( bool ) selectedItemsIds.push(id);

            else { // the item is being unselected
                // catchs its index
                let includesIndex = selectedItemsIds.indexOf(id);
                // range exception treatment, shoulnt happen, but never knows
                if (includesIndex > -1) 
                    // splice array out the items id
                    selectedItemsIds.splice(includesIndex, 1);
            }

            // immutable pratice
            return {
                ...prev, 
                selectedItemsIds
            }
        })
    }

    // toggle all items for actions
    toggleAllItems = () => {
        
        this.setState((prev:ProductListViewState)=>{
            let {selectedItemsIds, itemsData} = this.state;
    
            // if the selectedItemsIds is not all of then
            if(selectedItemsIds.length < itemsData.length){
                // select all items ids
                selectedItemsIds = itemsData.map( item => item.sku )
            }
            else{ // or the selectedItemsIds is already all of them
                // clean the selection
                selectedItemsIds = []
            }

            // immutable pratice
            return ({
                ...prev,
                selectedItemsIds
            })
        });

    }

    // mass delelte
    massDelete = async () => {
        // TODO
        // send the range of selected items ids to the server
        let {selectedItemsIds, itemsData} = this.state;
        
        if(selectedItemsIds.length==0){
            selectedItemsIds = itemsData.map( value => value.sku );
        }

        let postItems = JSON.stringify({
            "delete_list" : selectedItemsIds
        });
        const rawResponse = await fetch('api', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: postItems
        });

        console.log(postItems)
        const content = await rawResponse.json();
        if(content.length > 0){
            document.location.reload();
        }

    }

    // render time!
    render() {

        // maps itemsData through
        let renderedItems = this.state.itemsData.length > 0?
            this.state.itemsData.map( (itemData:Product) => 
                // calls the render item
                renderItem(
                    // with the checkboxstate
                    this.state.selectedItemsIds.includes(itemData.sku), 
                    // factory the toggle method
                    this.setCheckedFac(itemData.sku), 
                    // the item data itself
                    itemData
                ))
            // while is not fetching 
            : helpPage();

        // render page with:
        // 1- toggle all items method
        // 2- mass delete method
        // 3- if is all items selected
        // 4- the rendered items
        // 5- if any items are selected
        return renderPage(
            this.toggleAllItems, 
            this.massDelete,
            (this.state.selectedItemsIds.length === this.state.itemsData.length) && this.state.selectedItemsIds.length > 0, 
            renderedItems,
            this.state.selectedItemsIds.length>0
        );
    }

}

// redirect to add product
const redirectToAddProduct = () => window.location.assign("add-product");

// with the select all items method, the is all items selected verification and the items rendered list
const renderPage = (selectAllItems: () => void, massDelete: () => void, isAllItemsSelected: boolean, renderedItems: React.Component, isAnySelected: boolean) => 
    <React.Fragment>
        <div class="header">
            <h1 class="title">Product List</h1>
            <div class="controls">
                <button
                class="bx--btn bx--btn--primary"
                onClick={redirectToAddProduct}
                type="button">
                    ADD </button>
                <button
                class="bx--btn bx--btn--secondary"
                onClick={massDelete}
                // disabled={!isAnySelected}
                id="delete-product-btn"
                type="button">
                    MASS DELETE {isAnySelected? null: <small style={{marginLeft:10}}>(all)</small>}
                </button>
                <div class="bx--form-item bx--checkbox-wrapper">
                    <input onClick={selectAllItems} id="bx--checkbox-new" class="bx--checkbox" type="checkbox"
                    checked={isAllItemsSelected}
                    name="checkbox"/>
                    <label for="bx--checkbox-new" class="bx--checkbox-label">Mark all items</label>
                </div>
            </div>
        </div>

        <hr/>
        <div id="items">
            {renderedItems}
        </div>
    </React.Fragment>

// with the its-checked boolean, the set check state method and the item data
const renderItem = (isChecked:boolean, setChecked: (boolean) => void, item: Product) => {

    const checkToggle = () => setChecked(!isChecked);

    return (<div class="card bx--tile" onClick={checkToggle}>
        <span class="title">
            <div>
                <span class="name">{item["name"]}</span>
                <span class="price">
                    ${item["price"]}
                </span>
            </div>
            <span class="sku">{item["sku"]}</span>
        </span>
        <div class="detail">
            <span class="title">
                Details
            </span>
            <span>
                {item["detail_type"]}: {item["detail_value"]}
            </span>
        </div>
        <input  type="checkbox" onClick={checkToggle} checked={isChecked} class="delete-checkbox bx--checkbox" />
    </div>)
}


// render into #root element 
ReactDOM.render(
    <ProducListView/>,
    document.getElementById('root')
);

    