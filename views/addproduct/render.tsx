/*
    views\addProductPage\render.jsx is the reactjs script for the front end rendering and ui
    of the add product page. 
    Its atributions are:
    Render the form area with proper options
    Validate its fields (notify any erros or missing fields)
    Post to the rest /api

    in this file you will find:
    |   Types declarations
    |   Constant datas that drives the ui
    |   Render field method
    |   Add Product view class
    |   - Common fields handler
    |   - Details Type select field handler
    |   - Details dynamic fields handler
    |   - Save and post method
    |   --- Details field digest
    |   --- Unique sku validation
    |   --- 
*/

type AddProductViewState= {
    fields: {[key: string]: Field},
    subFields: {[key: string]: Field},
    backendError: string | null,
}

type Field = {
    id: string,
    label? : string,
    placeholder?: string,
    error?: string,
    hint?: string,
    value: string|null,
    mask? : (input: string) => string,
    validate?: (input:string) => boolean,
};

type Description = string;

type HandleCommonFieldState = (index: number) => (e: React.ChangeEvent<HTMLInputElement>) => void;

type DigestField = (input: Array<string>) => string;

const commonFields: {[key: string]: Field} = {
    "sku": {
        "id" : "sku",
        "label" : "SKU",
        "placeholder" : "Sku code",
        "error" : null,
        "value" : "",
        "validate" : (input) => /^[a-z0-9A-Z]{8,20}$/g.test(input),
        "hint" : "Invalid SKU code, try something like this: \"GGWP0007\""
    },
    "price": {
        "id" : "price",
        "label" : "Price",
        "placeholder" : "US $",
        "error" : null,
        "value" : "",
        "mask" : (input) => input[0] === '$'? input : '$'+input,
        "validate" : (input) => input.length ==0? true : /^\$?(\d{1,3}(\,\d{3})*|(\d+))(\.\d{2})?$/.test(input),
        "hint" : "Invalid currency value, it has to be a number, eg 0,000.00"
    },
    "name": {
        "id" : "name",
        "label" : "Name",
        "placeholder" : "Product name",
        "error" : null,
        "value" : "",
        "validate" : (input) => /^[a-z\s]{3,}$/i.test(input),
        "hint" : "Invalid Product name, minimum length of three, no special caracters"
    },
};

const typesForms: {[key: string] : [Description, Array<Field>, DigestField|null]} = {
    "DVD":[
        "Please, provide size", 
        [
            {
                "id" : "size",
                "label" : "Size (MB)",
                "placeholder" : "",
                "error" : null,
                "value" : "",
                "validate" : (input) => /^[0-9]*$/.test(input),
                "hint" : "Invalid height value, only digits"

            }
        ],
        (input: Array<string>) => {
            return input.join("")+" MB";
        }
    ],
    
    "Dimensions" : [
        "Please, provide dimensions in WxHxL format", 
        [
            {
                "id" : "height",
                "label" : "Height (CM)",
                "placeholder" : "",
                "error" : null,
                "value" : "",
                "validate" : (input) => /^[0-9]*$/.test(input),
                "hint" : "Invalid height value, only digits"
            },
            {
                "id" : "width",
                "label" : "Width (CM)",
                "placeholder" : "",
                "error" : null,
                "value" : "",
                "validate" : (input) => /^[0-9]*$/.test(input),
                "hint" : "Invalid height value, only digits"

            },
            {
                "id" : "length",
                "label" : "Length (CM)",
                "placeholder" : "",
                "error" : null,
                "value" : "",
                "validate" : (input) => /^[0-9]*\s?(CM|cm)?$/.test(input),
                "hint" : "Invalid height value, only digits"

            },
        ],
        (input: Array<string>) => {
            return input.join("x");
        }
    ],
    "Book": [
        "Please, provide weight", 
        [
            {
                "id" : "weight",
                "label" : "Weight (Kg)",
                "placeholder" : "",
                "error" : null,
                "value" : "",
                "validate" : (input) => /^[0-9]*$/.test(input),
                "hint" : "Invalid height value, only digits"

            }
        ],
        (input: Array<string>) => {
            return input.join("")+" Kg";
        }
    ]

}


const renderField = (field: Field, onChange: ((e: React.ChangeEvent<HTMLInputElement>) => void)) => {
    let {id, label, placeholder, error, value} = field;
    console.log(field)
    return (
        <div className="bx--form-item bx--text-input-wrapper">
            <label className="bx--label">{label} *required</label>
            <div className="bx--text-input__field-wrapper" data-invalid>
                <input id={id} type="text"
                value={value}
                onChange={onChange}
                className={`bx--text-input ${error!==null? 'bx--text-input--invalid': ''}`}
                placeholder={placeholder}/>
            </div>
            <div className="bx--form-requirement">
                {error}
            </div>
        </div>
    )
}


class AddProductView extends React.Component{
    // create a data scruture that strutures the fields 
    state:AddProductViewState = {
        fields : {...commonFields,
            "detail_type" : {
                id: "detail_type",
                value : null
            },
            "detail_value" : {
                id: "detail_value",
                value : null
            }
        },
        subFields : (typesForms["DVD"][1]).reduce(function(map, obj) {
            map[obj.id] = obj;
            return map;
        }, {}),
        backendError: null,
    }
    // and binds them with the errors messages

    // handle field change event
    handle = (index: string, obj: Field) => (e: React.ChangeEvent<HTMLInputElement>) => {
        // get value sent from event, get fields from state
        let {value} = e.target;
        let {fields} = this.state;

        console.log(index, obj)
        
        if(!obj.validate(value)){
            fields[index].error = obj.hint;
            fields[index].value = value;
        }
        else {
            fields[index].error = null;
            value = obj.mask? obj.mask(value) : value;
            fields[index].value = value;
        }
    
        // set state
        this.setState((prev:AddProductViewState) => ({
            ...prev,
            fields,
        }))
    }

    handleDetailsTypeOption: React.ChangeEventHandler<HTMLSelectElement> = (e) => {
        
        let {value} = e.target;
        let {fields} = this.state;
        
        fields["detail_type"] = {
            id: "detail_type",
            value
        };

        fields["detail_value"] = {
            id: "detail_value",
            value : ""
        }
            
        // set state
        this.setState((prev:AddProductViewState) => 
            ({...prev, fields, 
                subFields: (typesForms[fields["detail_type"].value][1]).reduce(function(map, obj) {
                    map[obj.id] = obj;
                    return map;
                }, {})}))
    }

    detailsValueHandler = (obj: Field) => (e) => {
        let {value} = e.target; 
        
        if(obj.validate(value)) {
            let field = {...this.state.subFields[obj.id], value, error: null};
            let {fields, subFields} = this.state; 
            let {detail_value} = fields;
            let digest = typesForms[fields["detail_type"].value][2];
            if(digest!==null){
                detail_value.value = digest(Object.values(subFields).map((s:Field) => s.value))
            }
            else{
                detail_value.value = value;
            }
            this.setState((prev:AddProductViewState) => {
                console.log(obj.id, field, fields);
                return {...prev, 
                    fields: {...this.state.fields, detail_value},
                    subFields: {...prev.subFields, [obj.id]: field}}
            });
        }

        else{
            let field = {...this.state.subFields[obj.id], value, error: obj.hint};
            console.log(field);

            this.setState((prev:AddProductViewState) => {
                return {...prev,
                    fields : {...prev.fields, detail_value: {...prev.fields.detail_value, value: null}},
                    subFields: {...prev.subFields, [obj.id]: field}}
            });
        }
    }

    // save method post product, show sucess notification 
    save = async () => {

        // generate clean fields data for posting
        let postItems: {[key: string] : string} = Object.keys(this.state.fields)
        .reduce( (acc, current) => {
            acc[current] = this.state.fields[current].value;
            return acc;
        }, {});

        // $ sign bug fix
        postItems['price'] = postItems['price'].replace('$', '');

        // unique sku verification
        let response = await fetch("/api?sku="+postItems.sku);
        let json = await response.json();

        if(json.length>0){
            // its already registered
            // set used sku error 
            this.setState((prev:AddProductViewState) => {
                let {fields} = prev;
                fields['sku'].error=" SKU code is already registered in our system, please, try a different one";
                return {
                    ...prev,
                    fields
                }
            });
            // exit function
            return;
        }

        // submit
        // and redirect to product list 
        const rawResponse = await fetch('/api', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(postItems)
        });

        const content = await rawResponse.json();

        // if everything went ok
        if(rawResponse.ok){
            if(content){
                // dynamicly redirect to index.php
                window.location.assign('/');
            }
            else{
                // fail post notification with error
                this.setState(prev => ({...prev, backendError: "An error ocurred posting the request, please try again or contact me!"}))
                console.log(rawResponse.statusText, content);
            }
        }
        else{
            // fail post notification with error
            console.log(content);
            this.setState(prev => ({...prev, backendError: "An error ocurred posting the request, please try again or contact me!"}))
        }

    }

    // cancel method, just redirect to product list page
    cancel = () => {
        // TODO: redirect to product list
        console.log("!")
    }
    render() {
        let commonFieldsRender = Object.entries(commonFields).map( (p:[string, Field]) => {
            return renderField(p[1], this.handle(p[0], p[1]))
        });

        console.log(this.state.subFields);
        let typeFormRender = this.state.subFields && (this.state.fields["detail_type"].value!==null)?
            <div>
                {Object.values(this.state.subFields).map(field => 
                    renderField(field, this.detailsValueHandler(field))
                )}
                <p>{typesForms[this.state.fields["detail_type"].value][0]}</p>
            </div>
        : null; 

        const allValidated = Object.values(this.state.fields).reduce( (p, c) => {
            if (c.value == null) return false;
            return ( p && (c.error==null && c.value.length>0))
        }, true );
            // .reduce( (f: Field) => f.error == null))

        return (<React.Fragment>
            <div className="header">
                <h1 className="title">Product Add</h1>
                <div className="controls">
                    
                    {this.state.backendError && 
                    <div className="bx--form-requirement db-error">
                        {this.state.backendError}
                    </div>}

                    <button
                    className="bx--btn bx--btn--primary"
                    onClick={this.save}
                    disabled={!allValidated}
                    type="button">
                        Save
                    </button>
                    <button
                    className="bx--btn bx--btn--secondary"
                    onClick={()=> window.location.assign('/')}
                    type="button">
                        Cancel
                    </button>
                </div>
                
            </div>
            
            <form id="product_form" >
                <div class="common-fields">
                    {commonFieldsRender}
                </div>
                <div className="details-form">
                    <div
                        className="bx--select">
                        <label className="bx--label">Select label</label>
                        <div className="bx--select-input__wrapper" >
                            <select id="productType" className="bx--select-input" onChange={this.handleDetailsTypeOption}>
                                <option className="bx--select-option" value=""  disabled selected hidden>
                                    Choose an type
                                </option>
                                <option className="bx--select-option" value="DVD" >
                                    DVD
                                </option>
                                <option className="bx--select-option" value="Book" >
                                    Book
                                </option>
                                <option className="bx--select-option" value="Dimensions" >
                                    Furniture
                                </option>
                            </select>
                        </div>
                    </div>
                    {typeFormRender}
                </div>
            </form>
            
        </React.Fragment>);
    }
}

// create a fn component that render the field based on struture

// render into element
ReactDOM.render(
    <AddProductView/>,
    document.getElementById('root')
);