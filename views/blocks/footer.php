
<footer>
    <hr id="fromFooter"/>
    <span>
        Scandiweb Test assignment
    </span>
    <div class="icaroglauco">
        <a href="http://linkedin.com/in/dev-icaroglauco/"><i class="fab fa-linkedin"></i> in/dev-icaroglauco/</a>
        <a href="http://fb.com/icaro.glauco"><i class="fab fa-facebook-square"></i> icaro.glauco</a>
        <a href="http://github.com/icaroglauco-lab"><i class="fab fa-github-alt"></i> icaroglauco-lab</a>
    </div>
</footer>

<style type="text/css">
    footer{
        margin-top: 10vh;
        display: grid;
        position: relative;
        bottom: 0%;
        grid-gap: 15px;
        width: 100%;
        padding-bottom: 2vh;
    }

    footer > *{
        margin-left: 5em;
    }

    footer a{
        color: #333;
        text-decoration: none;
    }
    
    footer > .icaroglauco{
        display: grid;
        height: 60px;
    }
    
    #fromFooter{
        margin-bottom: 1em;
        margin-left: 0px
    }
    
</style>

</body>
</html>